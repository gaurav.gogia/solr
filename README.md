Set-up a Solr Server on local machine in Kubernetes Cluster

Machine : Macos


STEP 1 : Install Minikube (In order to set up Minikube on Mac, first install kubectl binary). 

A) Install kubectl binary with curl on macOS


# curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl"

Make the kubectl binary executable.

	# chmod +x ./kubectl

Move the binary in to your PATH.

	# sudo mv ./kubectl /usr/local/bin/kubectl

To check the version of kubectl 

    # kubectl version


B) Install a hypervisor , in my case I have used VirtualBox


C) Install Minikube

	  # brew install minikube


Add the Minikube executable to your path:

	 # sudo mv minikube /usr/local/bin

	 Confirm Installation.

	 1. minikube start --vm-driver=<driver_name>    
	   
	 2. Check the status of Minikube Cluster

      # minikube status



STEP 2: Create Statefulset object using solr.yaml

A) run “kubectl apply -f solr.yaml”

# This will create solr database pod running on port 8983 along with persistent and persistent claim volume. Also create an headless service which is used to track the Network Ip of running solr pods. 


# Use following commands to check the status of objects we created: 

            Kubectl get pods - “Results in Displaying running pods”
   
            Kubectl get statfulset object “Display status of statefulset object ”
          
            Kubectl get svc “Display corresponding service created”


#You can also check Persistant volume and Persistent Volume Claim defined while creating the object

A PersistentVolume (PV) is a piece of storage in the cluster that has been provisioned by an administrator or dynamically provisioned using storage classes.
 
           Kubectl get pv 


A PersistentVolumeClaim (PVC) is a request for storage by a user. It is similar to a Pod. Pods consume node resources and PVCs consume PV resources
  
          Kubectl get pvc - "Assigned storage defined under volumeClaimTemplates in solr.yaml"



# Create database inside solr in order to push and retrieve data from solr:

 kubectl exec -it “pod-name” /bin/bash


 solr create -c test_core “Create test_core database”


Step 3: Create Nginx DeploymentConfig Using DeploymentConfig.yaml


A) run “kubectl apply -f DeploymentConfig.yaml”

  # It will create Nginx Pod along with Confgimap(Containing Nginx Configuration) and expose nginx service on NodePort(30,000 - 32,767)


  # check the results by executing following commands :


   Kubectl get pods 

   Kubectl get deployments

   Kubectl get svc  

   Kubectl get configmap


B) Push and Fetch data from Solr DB 


Now make curl request on Nginx Service in order to fetch the result from solr


1. Curl for Push data into solr :
         curl 'http://<POD IP> or <service/ip>:8983/solr/test_core/update?commitWithin=1000&overwrite=true&wt=json' -H 'Accept: application/json, text/plain, /' -H 'Origin: http://localhost:8983' -H 'Content-type: application/json' --data-binary $'[{\n  id: 2\n  name: \’Test\’\n}]’ -i 


2. Curl for Fetching the result from solr :
         curl 'http://<POD IP or servicename/ip>:8983/solr/test_core/select?q=id%3A2' -i


Note: Use Port-Forwarding in case minikube is running on hypervisor like Virtual Machine in order to allow traffic from local machine to pod. 
Like in my case I have access the pod from local machine via following :


kubectl port-forward <Pod Name> 3000:80
